import java.util.ArrayList;
import java.util.Scanner; // Import the Scanner class
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class KNN {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in); // Create a Scanner object

        boolean caminhoValido = false;

        InputStream input;
        String line = null;
        int numeroAmostras = 0;
        // Caminho das amostras
        while (!caminhoValido) {
            System.out.println("Entre com o caminho do arquivo contendo as amostras: ");
            String caminho = "/Users/Shared/Rec de Padroes/reconhecimentodepadroes2019-knn/Trabalho1/data/dados.txt"; // myObj.nextLine();
                                                                                                                      // //ex.:
                                                                                                                      // /Users/Shared/Rec
                                                                                                                      // de
                                                                                                                      // Padroes/reconhecimentodepadroes2019-knn/Trabalho1/data/dados.txt
            try {
                input = new FileInputStream(caminho);
                BufferedReader buff = new BufferedReader(new InputStreamReader(input));
                while (buff.ready()) {
                    line = buff.readLine();
                    System.out.println(line);
                    if (numeroAmostras > 0) {// pula primeira linha
                        String[] splitStr = line.split("\\s+");
                        Amostra temp = new Amostra(splitStr);
                    }
                    numeroAmostras++;// incrementa numero de amostras
                }
                caminhoValido = true;
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(">>>>>>>>>>>>>>>>>>>\nFalha na leitura do arquivo!");
                System.out.println("Caminho inválido. Tente novamente\n");
            }
        }
        numeroAmostras--;// desconsidera primeira linha de referencia

        // define K vizinhos
        int k;
        boolean kValido = false;
        while (!kValido) {
            System.out.println("Entre com o numero de vizinhos (k): ");
            String kStr = myObj.nextLine();
            try {
                k = Integer.parseInt(kStr);
                if (k <= numeroAmostras && k > 1) {
                    kValido = true;
                } else {
                    System.out.println(
                            "\n\n>>>>>>k precisa ser um número maior que zero e menor ou igual ao número de amostras ("
                                    + numeroAmostras + "):\n");
                }
            } catch (Exception e) {
                System.out.println("\n\n>>>>>>k precisa ser um número inteiro. Tente novamente:\n");
            }
        }
        // define particoes do K-fold
        int kFold = 0;
        boolean kFoldValido = false;
        while (!kFoldValido) {
            System.out.println("Entre com o número de partições k-fold: ");
            String kStr = myObj.nextLine();
            try {
                kFold = Integer.parseInt(kStr);
                if (kFold <= numeroAmostras) {
                    if (kFold >= 2) {
                        kFoldValido = true;
                    } else {
                        System.out.println("\n\n>>>>>>k precisa ser um número divisivel pelo numero de amostras "
                                + numeroAmostras + ":\n");
                    }
                } else {
                    System.out.println("\n\n>>>>>>k precisa ser um número menor ou igual ao número de amostras ("
                            + numeroAmostras + "):\n");
                }
            } catch (Exception e) {
                System.out.println("\n\n>>>>>>k precisa ser um número inteiro. Tente novamente:\n");
            }
        }

        int[] tamanhos = new int[kFold];// aloca vetor com os tamanhos de cada fold k

        int tamPadrao = (int) Math.floor(numeroAmostras / kFold);
        int restante = numeroAmostras % kFold;
        System.out.println("Serão " + kFold + " folds contendo os seguintes tamanhos:\n");

        //Distribui as amostras de modo que cada fold fique com tamanho mais proximo um do outro
        for (int i = 0; i < kFold; i++) {
            tamanhos[i] = tamPadrao;
            if (restante > 0) {
                tamanhos[i]++;
                restante--;
            }
            System.out.println(tamanhos[i]);
        }

        int idx= 0;
        Amostra[] amostras= new Amostra[kFold];

        for (int i = 0; i < kFold; i++) { // itera em cada fold
            for(int j= idx; j< idx + tamanhos[i] -1; j++){ //itera em cada amostra do fold
                //Amostra temp= amostras[j];
            }
            //idx+= tamanhos[i]-1;
        }

        myObj.close();
        // Além disso, o usuário poderá selecionar se deseja normalizar os dados (com o
        // algoritmo
        // z-score) antes de aplicar o classificador
        System.out.println(
                "Deseja normalizar os dados (com o algoritmo z-score) antes de aplicar o classificador? (Y/N)\n");

    }
}